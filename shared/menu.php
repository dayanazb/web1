

<?php
$search = isset($_GET['search']) ? $_GET['search'] : '';
$categoria = $categoria_model->find();
?>

<!DOCTYPE html>
<html>
<head>
  <title></title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>
  <body>
  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Compra en línea</a>
    </div>
    <ul class="nav navbar-nav">
      <li>
        <a href="/home/index.php">Principal</a>
      </li>

      <li>
        <a href="/categorias/vista.php">Categorias</a>
      </li>

      <li><a href="/carritos">Carrito Compra</a></li>

       <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Configuración
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
           <?php $t_usuario = strcmp($_SESSION['tipo_usuario'],"f"); ?>
            <?php if($t_usuario == 0) : ?>
               <li><a  href="/shared/pagina_vacia.php">Categoría</a></li>
               <li><a href="/shared/pagina_vacia.php">Articulo</a></li>
            <?php else : ?>
              <li><a href="/categorias">Categoría</a></li>
              <li><a href="/articulos">Articulo</a></li> 
              <?php endif; ?>
        </ul>
      </li>
      <li><a href="/checkout">Checkout</a></li>
      <li><a href="/consulta_orden">Consultar Compras</a></li>
    </ul>

    <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><?= $_SESSION['nombre'] ?></a></li>
        <li>
          <a href="/seguridad/logout.php" onclick="return confirm('¿Esta seguro que desea cerrar sesión?')">Cerrar Sesión</a>
        </li>
    </ul>

  </div>
</nav>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>





