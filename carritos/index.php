<?php
include '../seguridad/verificar_session.php';
$search = isset($_GET['search']) ? $_GET['search'] : '';

  include '../DbSetup.php';
  ?>
  
<!DOCTYPE html>
<html>
<head>
  <title>Página php</title>
  <meta charset="utf-8">
</head>
<body>
  <?php include '../shared/menu.php'; ?>
  <div class="container">
    <h3 align="center">Carrito de Compra</h3>
    <br />
    <table class="table table-striped">
      <tr>
        <th>ID</th>
        <th>USUARIO</th>
        <th>ARTICULO</th>
        <th>PRECIO</th>
        <th>CANTIDAD</th>
        <th> <a href="/categorias/vista.php">Atras</a></th>
      </tr>
      <?php
        include '../DbSetup.php';
        $result_array = $carrito_model->index($search); 
        if(!empty($result_array)){
        foreach ($result_array as $row) {
           //if(isset($row['id']) ? $row['id'] : ''){
          echo "<tr>";
            echo "<td>" . $row['id'] . "</td>";
            echo "<td>" . $row['id_usuario'] . "</td>";
            echo "<td>" . $row['descripcion'] . "</td>";
            echo "<td>" ."$". $row['precio'] . "</td>";
            echo "<td>" . $row['cantidad'] . "</td>";
            echo "<td>" .
                  "<a href='/carritos/delete.php?id=" . $row['id'] . "'>Eliminar</a>".
                  "</td>";         
          echo "</tr>";
        }
        }else{
          echo "No hay ningun articulo en el carrito";
        }
      ?>
    </table>
  </div>

</body>
</html>
