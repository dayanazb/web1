<?php
  include '../DbSetup.php';
  $id = $_GET['id'];
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $nombre = $_POST['nombre'];
    $carrito_model->update($id, $nombre);
    return header("Location: /Shopcar");
  }
  $carrito = $carrito_model->find($id);
?>
<!DOCTYPE html>
<html>
<head>
   <?php include '../shared/menu.php'; ?>
  <title>Editar Carrito de compras</title>
</head>
<body>
  <h3>Editar Carrito de compras</h3>
  <form method="POST">
    <label>Nombre:</label>
    <input type="text" name="nombre" required autofocus value="<?php echo $carrito['nombre']?>">
    <input type="submit" value="Salvar">
    <a href="/Shopcar">Atras</a>
  </form>
  <?php include '../shared/footer.php'; ?>
</body>
</html>
