<?php

require_once '../Db/BaseConnection.php';
require_once '../Db/PostgresConnection.php';
require_once '../Db/MySqlConnection.php';
require_once '../Models/Usuario.php';
require_once '../Models/Categoria.php';
require_once '../Models/Articulo.php';
require_once '../Models/Carrito.php';
require_once '../Models/Orden.php';
/*
    Polimorfismo:
    un objeto puede tomar varias formas y funcionar diferente en cada una de sus formas.

    Ejemplo:
    el objeto $connection puede conectarse a mysql y a postgres simplemente cambiando la variable $db_class
*/

$db_class = getenv('DB_CLASS');
$connection = new $db_class(
              getenv('SERVER'),
              getenv('PORT'),
              getenv('USER'),
              getenv('PASSWORD'),
              getenv('DATABASE'));
$connection->connect();
$usuario_model = new Models\Usuario($connection);
$categoria_model = new Models\Categoria($connection);
$articulo_model = new Models\Articulo($connection);
$carrito_model = new Models\Carrito($connection);
$orden_model = new Models\Orden($connection);
/*
$result = $connection->executeSql('select table_name from information_schema.tables limit 5');
$result_array =  $connection->getResults($result);

foreach ($result_array as $row) {
  echo "nombre: " . $row['table_name'] . "\n";
}

$connection->disconnect();*/
