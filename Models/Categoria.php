<?php

namespace Models {

  class Categoria
  {
    private $connection;
    function __construct($connection)
    {
      $this->connection = $connection;
    }

    public function find_for_id($id)
    {
      $result = $this->connection->executeSql("select * from public.categoria where id = $id");
      return $this->connection->getResults($result)[0];
    }

    public function find()
    {
      $result = $this->connection->executeSql("select * from public.categoria");
      return $this->connection->getResults($result);
    }

    public function index($search)
    {
      $sql = "select * from public.categoria ";
      if ($search) {
        $search_criteria = [];
        array_push($search_criteria, "id = " . intval($search));
        array_push($search_criteria, "descripcion ilike '%" . $search ."%'");
        $sql .= " where " . join($search_criteria, ' or ');
      }
      $sql .= "order by id";
      $result = $this->connection->executeSql($sql);
      return $this->connection->getResults($result);
    }
  

    public function insert($descripcion)
    {
      $sql = "INSERT INTO public.categoria(descripcion) VALUES ('$descripcion')";
      $this->connection->executeSql($sql);
    }

    public function update($id, $descripcion)
    {
      $sql = "UPDATE public.categoria SET descripcion = '$descripcion' WHERE id = $id";
      $this->connection->executeSql($sql);
    }

    public function delete($id)
    {
      $sql = "DELETE FROM public.categoria WHERE id = $id";
      $this->connection->executeSql($sql);
    }
  }
}
