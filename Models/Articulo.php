<?php

namespace Models {

  class Articulo
  {
    private $connection;
    function __construct($connection)
    {
      $this->connection = $connection;
    }

    public function find($id)
    {
      $result = $this->connection->executeSql("select * from public.articulo where id = $id");
      return $this->connection->getResults($result)[0];
    }

    public function findArticulos()
    {
      $result = $this->connection->executeSql("select * from public.articulo");
      return $this->connection->getResults($result);
    }
    

    public function index1($search, $id)
    {
      $sql = "select * from public.articulo where id_categoria = $id";
      if ($search) {
        $search_criteria = [];
        array_push($search_criteria, "id = " . intval($search));
        array_push($search_criteria, "descripcion ilike '%" . $search ."%'");
        array_push($search_criteria, "precio = " . numericval($search));
        array_push($search_criteria, "cantidad = " . doubleval($search));
        array_push($search_criteria, "imagen ilike '%" . $search ."%'");
        $sql .= " where " . join($search_criteria, ' or ');
      }
      $sql .= " order by id";
      $result = $this->connection->executeSql($sql);
      return $this->connection->getResults($result);
    }



    public function index($search)
    {
      $sql = "select * from public.articulo";
      if ($search) {
        $search_criteria = [];
        array_push($search_criteria, "id = " . intval($search));
        array_push($search_criteria, "descripcion ilike '%" . $search ."%'");
        array_push($search_criteria, "precio = " . intval($search));
        array_push($search_criteria, "cantidad = " . doubleval($search));
        array_push($search_criteria, "imagen ilike '%" . $search ."%'");
        $sql .= " where " . join($search_criteria, ' or ');
        array_push($search_criteria, "id_categoria = " . intval($search));
      }
      $sql .= " order by id";
      $result = $this->connection->executeSql($sql);
      return $this->connection->getResults($result);
    }
   
    

    public function insert($descripcion, $precio, $cantidad, $imagen, $categoria)
    {
      $sql = "INSERT INTO public.articulo(descripcion,precio,cantidad,imagen,id_categoria) VALUES ('$descripcion','$precio', '$cantidad','$imagen','$categoria')";
      $this->connection->executeSql($sql);
      
    }

    public function update($id, $descripcion, $precio, $cantidad, $nombre_imagen, $categoria)
    {
      $sql = "UPDATE public.articulo SET descripcion = '$descripcion', precio = '$precio', cantidad = '$cantidad', imagen = '$nombre_imagen', id_categoria = '$categoria' WHERE id = $id";
      $this->connection->executeSql($sql);
    }

    public function delete($id)
    {
      $sql = "DELETE FROM public.articulo WHERE id = $id";
      $this->connection->executeSql($sql);
    }

   
  }
}
