<?php

namespace Models {

  class Carrito
  {
    private $connection;
    function __construct($connection)
    {
      $this->connection = $connection;
    }

    public function find($id)
    {
      $result = $this->connection->executeSql("select * from carrito_compra where id = $id");
      return $this->connection->getResults($result)[0];
    }

    public function index($search)
    {
      $sql = "SELECT carrito_compra.id, carrito_compra.id_usuario, 
              articulo.descripcion, articulo.precio, articulo.cantidad 
              FROM(carrito_compra INNER JOIN articulo ON carrito_compra.id_articulo = articulo.id) WHERE id_usuario = " . $_SESSION["usuario_id"];
      
      $result = $this->connection->executeSql($sql);
      return $this->connection->getResults($result);
    }


    public function sumatoria()
    {
      $result = $this->connection->executeSql("SELECT precio from public.articulo WHERE id = " . $_SESSION["usuario_id"]);
      return $this->connection->getResults($result)[0];
    }


    public function insert($id_usuario,$id_articulo)
    {
      $sql = "INSERT INTO public.carrito_compra(id_usuario,id_articulo) VALUES ('$id_usuario','$id_articulo')";
      $this->connection->executeSql($sql);
    }

    public function update($id, $descripcion,$id_usuario,$id_articulo)
    {
      $sql = "UPDATE carrito_compra SET descripcion = '$descripcion' AND id_usuario = '$id_usuario' AND id_articulo = '$id_articulo'  WHERE id = $id";
      $this->connection->executeSql($sql);
    }


     public function delete_carrito($id_usuario)
    {
      $sql = "DELETE FROM public.carrito_compra WHERE id_usuario = $id_usuario";
      $this->connection->executeSql($sql);
    }


    public function deleteArticulo($id)
    {
      $sql = "DELETE FROM public.carrito_compra WHERE id = $id";
      $this->connection->executeSql($sql);
    }
  }
}
