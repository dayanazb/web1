<?php
  include '../seguridad/verificar_session.php';
  include '../DbSetup.php';
  $id = $_GET['id'];
  $categoria = $categoria_model->find_for_id($id);
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $categoria_model->delete($id);
    return header("Location: /categorias");
  }
?>
<!DOCTYPE html>
<html>
<head>
  <title>Eliminar Categorías</title>
</head>
<body>
  <div class="container">
    <h3>Eliminar Categoría</h3>
    <p>
      Esta seguro de eliminar la categoría: <strong><?php echo $categoria['descripcion']; ?></strong>
    </p>
    <form method="POST">
      <input type="submit" value="Si">
      <a href="/categorias">No</a>
    </form>
</div>
</body>
</html>

