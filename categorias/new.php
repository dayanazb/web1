<?php
  include '../seguridad/verificar_session.php';
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    include '../DbSetup.php';
    $descripcion = isset($_POST['descripcion']) ? $_POST['descripcion'] : '';
    $categoria_model->insert($descripcion);
    header("Location: /categorias");
  }
?>
<!DOCTYPE html>
<html>
<head>
   <?php include '../shared/menu.php'; ?>
  <title>Nuevo Categoría</title>
</head>
<body>
  <div class="container">
    <h3>Nuevo Categoría </h3>
    <form method="POST">
      <label>Categoría:</label>
      <input type="text" name="descripcion" required autofocus>
      <br />
      <input type="submit" value="Salvar">
      <a href="/categorias">Atras</a>
    </form>
</div>

</body>
</html>
