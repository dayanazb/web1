<?php
include '../seguridad/verificar_session.php';
?>
<!DOCTYPE html>
<html>
<head>
  <title>Página php</title>
  <meta charset="utf-8">
</head>
<body>
  <?php include '../shared/menu.php'; ?>
  <div class="container">
    <h3 align="center">Categorias</h3>
    <br />
    <table class="table">
      <tr>
        <th>Id</th>
        <th>Categoría</th>
      </tr>
      <?php
        include '../DbSetup.php';
        $result_array = $categoria_model->find();
        if(!empty($result_array)){
        foreach ($result_array as $row) {
          echo "<tr>";
            echo "<td>" . $row['id'] . "</td>";
            echo "<td>" . $row['descripcion'] . "</td>";
            echo "<td>" .
                  "<a href='/categorias/ver.php?id=" . $row['id'] . "'>Ver</a>".
                  "</td>";
          echo "</tr>";
        }
      }else{
        echo "No hay artículos";
      }
      ?>
    </table>
</div>

</body>
</html>

