<?php
  include '../seguridad/verificar_session.php';
  include '../DbSetup.php';
  $id = $_GET['id'];
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $descripcion = $_POST['descripcion'];
    $categoria_model->update($id, $descripcion);
    return header("Location: /categorias");
  }
  $categoria = $categoria_model->find_for_id($id);
?>
<!DOCTYPE html>
<html>
<head>
  <title>Editar Categoría</title>
</head>
<body>
  <div class="container">
    <h3>Editar Categoría</h3>
    <form method="POST">
      <label>Categoría:</label>
      <input type="text" name="descripcion" required autofocus value="<?php echo $categoria['descripcion']?>">
      <input type="submit" value="Salvar">
      <a href="/categorias">Atras</a>
    </form>
 </div>

</body>
</html>
