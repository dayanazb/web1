<?php
include '../seguridad/verificar_session.php';
$search = isset($_GET['search']) ? $_GET['search'] : '';
?>
<!DOCTYPE html>
<html>
<head>
  <title>Página php</title>
  <meta charset="utf-8">
</head>
<body>
  <?php include '../shared/menu.php'; ?>
  <div class="container">
    <h3 aling="center">Categorias</h3>
    <br />
    <form  method="GET" >
    </form>
    
      <table  class="table table-striped">
        <tr>
          <th>Id</th>
          <th>Categoría</th>
          <th><a href="/categorias/new.php">+</a></th>
        </tr>
        <?php
          include '../DbSetup.php';
          $result_array = $categoria_model->find();
          
          if(!empty($result_array)){
          foreach ($result_array as $row) {
            echo "<tr>";
              echo "<td>" . $row['id'] . "</td>";
              echo "<td>" . $row['descripcion'] . "</td>";
              echo "<td>" .
                   "<a href='/categorias/edit.php?id=" . $row['id'] . "'>Editar</a>". "   " .
                    "<a href='/categorias/delete.php?id=" . $row['id'] . "'>Eliminar</a>".
                    "</td>";
            echo "</tr>";
          }
        }else{
          echo "No existe esa categoria";
        }
        ?>
      </table>
</div>

</body>
</html>

