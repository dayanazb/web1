<?php
      
  $titulo = 'Login';

  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    include '../DbSetup.php';
    $email = isset($_POST['email']) ? $_POST['email'] : '';
    $password = isset($_POST['password']) ? $_POST['password'] : '';
    $usuario = $usuario_model->find($email, $password);
    if (isset($usuario)) {
      session_start();
      $_SESSION['usuario_id'] = $usuario['id'];
      $_SESSION['nombre'] = $usuario['nombre'];
      $_SESSION['tipo_usuario'] = $usuario['tipo_usuario'];
      return header("Location: /home");
    } else {
      echo "<h3>Usuario o contraseña invalido</h3>";
    }
  }else{
  }
  include '../shared/header.php';
?>
  <form method="POST">
    <label>Email: </label>
    <input type="email" name="email" value="<?= isset($_POST['email']) ? $_POST['email'] : ''; ?>">
    <br>
    <label>Contraseña:</label>
    <input type="password" name="password">
    <br>
    <input type="submit" name="" value="Login!">
    <a href="/seguridad/registro.php">Registrarse</a>
  </form>
<?php
include '../shared/footer.php';
