<?php
  $titulo = 'Registro';
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    include '../DbSetup.php';
    $email = isset($_POST['email']) ? $_POST['email'] : '';
    $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : '';
    $password = isset($_POST['password']) ? $_POST['password'] : '';
    $tipo_usuario = isset($_POST['tipo_usuario']) ? $_POST['tipo_usuario'] : '';
    $password_confirmation = isset($_POST['password_confirmation']) ? $_POST['password_confirmation'] : '';
    if ($password != $password_confirmation) {
      echo "<h3>Las contraseñas no coinciden</h3>";
    } else {
      $usuario_model->insert($email, $nombre, $password,$tipo_usuario);
      echo "<h3>Usuario registrado con éxito</h3>";
      return header("Location: /seguridad/login.php?id=" . $row['id']);
    }
  }
  include '../shared/header.php';
?>
  <form method="POST">
    <label>Email: </label>
    <input type="email" name="email">
    <br>
    <label>Nombre: </label>
    <input type="text" name="nombre">
    <br>
    <label>Contraseña:</label>
    <input type="password" name="password">
    <br>
    <label>Confirmar Contraseña:</label>
    <input type="password" name="password_confirmation">
    <br>
    <label>Tipo Usuario:</label>
    <input type="checkbox" name="tipo_usuario" value=true>Administrador</input>
    <input type="checkbox" name="tipo_usuario" value=false>Comprador</input>
    <br>
    <input type="submit" name="" value="Registrarme!">
  </form>
<?php
include '../shared/footer.php';
?>

