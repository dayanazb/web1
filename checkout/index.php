<?php
include '../seguridad/verificar_session.php';
$search = isset($_GET['search']) ? $_GET['search'] : '';
$precio=0;
  include '../DbSetup.php';
  ?>
  
<!DOCTYPE html>
<html>
<head>
  <title>Página php</title>
  <meta charset="utf-8">
</head>
<body>
  <?php include '../shared/menu.php'; ?>

  <div class="container">
    <h3>Carrito de Compra</h3>
    <table class="table table-striped">
      <tr>
        <th>USUARIO</th>
        <th>ARTICULO</th>
        <th>PRECIO</th>
        <th>CANTIDAD</th>
      </tr>
      <?php
        include '../DbSetup.php';
        $result_array = $carrito_model->index($search);
        if(!empty($result_array)){
        foreach ($result_array as $row) {
          echo "<tr>";
            echo "<td>" . $row['id_usuario'] . "</td>";
            echo "<td>" . $row['descripcion'] . "</td>";
            echo "<td>" ."$". $row['precio'] . "</td>";
            $precio +=$row['precio']; 
            echo "<td>" . $row['cantidad'] . "</td>";       
          echo "</tr>";
        }
      }else{
          echo "No existen compras";
        }
         ?>
    </table>
   <?php echo "<tr>";
         echo "<td>". "Total a pagar:" . $precio . "</td>";     
          echo "<td>" .
                  "<a href='/checkout/orden.php?precio=" . $precio . "'>Comprar</a>".
                  "</td>";
                  
     echo "</tr>";
      ?>
  </div>
</body>
</html>
