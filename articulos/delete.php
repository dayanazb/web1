<?php
 include '../seguridad/verificar_session.php';
  include '../DbSetup.php';
  $id = $_GET['id'];
  $articulo = $articulo_model->find($id);
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $articulo_model->delete($id);
    return header("Location: /articulos");
  }
?>
<!DOCTYPE html>
<html>
<head>
  <?php include '../shared/menu.php'; ?>
  <title>Eliminar Articulo</title>
</head>
<body>
  <div class="container">
    <h3>Eliminar Articulo</h3>
    <p>
      Esta seguro de eliminar el articulo: <strong><?php echo $articulo['descripcion']; ?></strong>
    </p>
    <form method="POST">
      <input type="submit" value="Si">
      <a href="/articulos">No</a>
    </form>
</div>
</body>
</html>
