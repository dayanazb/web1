<?php
  include '../seguridad/verificar_session.php';
  include '../DbSetup.php';
  $id = $_GET['id'];
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $descripcion = $_POST['descripcion'];
    $precio = $_POST['precio'];
    $cantidad = $_POST['cantidad'];
    $categoria =$_POST['categoria'];
    //Imagen
    $nombre_imagen=$_FILES['imagen']['name'];
    $tipo_imagen=$_FILES['imagen']['type'];
    $tamanno_imagen=$_FILES['imagen']['size'];

    if($tamanno_imagen<=3000000){
      if($tipo_imagen=="image/jpeg" || $tipo_imagen=="image/png" || $tipo_imagen=="image/jpg" || $tipo_imagen=="image/gif"){
        $ruta =$_SERVER['DOCUMENT_ROOT'].'/imagenes/';
        move_uploaded_file($_FILES['imagen']['tmp_name'], $ruta.$nombre_imagen);
      }
      else{
        echo "Solo se pueden subir imagenes";
      }     
    }else{
      echo "El tamaño es demasiado grande";
    }
    $articulo_model->update($id,$descripcion,$precio,$cantidad,$nombre_imagen,$categoria);
       header("Location: /articulos");
   }

  $articulo = $articulo_model->find($id);
?>
<!DOCTYPE html>
<html>
<head>
   <?php include '../shared/menu.php'; ?>
  <title>Editar Articulo</title>
</head>
<body>
  <div class="container">
    <h3 align="center">Editar Articulo</h3>
    <form method="POST" enctype="multipart/form-data" >
      <table align="center" class="table">
        <tr>
          <td>
            <label>Descripción:</label>
          </td>
          <td><input type="text" name="descripcion" required autofocus value="<?php echo $articulo['descripcion']?>"></td>
        </tr>
        <tr>
          <td>
            <label>Precio:</label>
          </td>
          <td><input type="number" name="precio" required autofocus value="<?php echo $articulo['precio']?>"></td>
        </tr>
        <tr>
          <td>
            <label>Cantidad:</label>
          </td>
          <td><input type="number" name="cantidad" required autofocus  value="<?php echo $articulo['cantidad']?>"></td>
        </tr>
        <tr>
          <td>
            <label>Categoría:</label>
          </td>
          <td>
            <?php 
            include '../DbSetup.php'; 
             $result_array = $categoria_model->find();
             echo '<select name="categoria">';
            foreach ($result_array as $row) {
              echo '<option value="'.$row[id].'">'.$row[descripcion].'</option>';
            }
            echo '</select>';
            ?> 
        
        </td>
        <tr>
          <td>
            <label for="imagen">Imagen:</label>
          </td>
          <td><input type="file" name="imagen" size="20"></td>
        </tr>
        <tr><td align="center"><input type="submit" value="Guardar">
        <a href="/articulos">Atras</a></td></tr>
    </form>
</div>

</body>
</html>
