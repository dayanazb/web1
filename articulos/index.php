<?php
include '../seguridad/verificar_session.php';
$search = isset($_GET['search']) ? $_GET['search'] : '';
?>
<!DOCTYPE html>
<html>
<head>
  <title>Página php</title>
  <meta charset="utf-8">
</head>
<body>
  <?php include '../shared/menu.php'; ?>

  <div class="container">
    <h3 align="center">Articulos</h3>
    <form method="GET">
    </form>
    <table  class="table table-striped">
      <tr>
        <th>ID</th>
        <th>DESCRIPCION</th>
        <th>PRECIO</th>
        <th>CANTIDAD</th>
        <th>CATEGORIA</th>
        <th>IMAGEN</th>
        <th><a href="/articulos/new.php">+</a></th>

      <?php
        include '../DbSetup.php';
        $result_array = $articulo_model->findArticulos();
        if(!empty($result_array)){
        foreach ($result_array as $row) {
          echo "<tr>";
            echo "<td>" . $row['id'] . "</td>";
            echo "<td>" . $row['descripcion'] . "</td>";
            echo "<td>" ."$". $row['precio'] . "</td>";
            echo "<td>" . $row['cantidad'] . "</td>";
            echo "<td>" . $row['id_categoria']. "</td>";
            echo "<td>"  . "<img style=\"width: 22%;\" src='/imagenes/".$row['imagen'] . "'>" ."</td>";
            echo "<td>" .
            
                  "<a href='/articulos/edit.php?id=" . $row['id'] . "'>Editar</a>". "   " .
                  "<a href='/articulos/delete.php?id=" . $row['id'] . "'>Eliminar</a>".
                  "</td>";
           
          echo "</tr>";
        }
         }else{
          echo "No existen artículos";
         }
      ?>
      

    </table>
  </div>
</body>
</html>
